from tools.console import *
from chapitres.chapitre6.exo1_a_10 import *

def chapitre6():
    print("\n\t Cette partie du devoir concerne les Tests en Python...\n")
    while True :
        choix_fait=input("""\t\t Entrez le numero de l'exercice que vous voulez lancer 
                
                 1. Jours de la Semaine
                 2. Séquence Complémentaire d'un Brin d'ADN
                 3. Minimum d'une liste
                 4. Fréqunce d'Acide Aminés
                 5. Notes et Mentions d'un étudiant
                 6. Nombres Pairs
                 7. Conjecture de Syracuse
                 8. Attribution de la structure secondaire des acides aminés d'une protéine
                 9. Détermination de nombre premiers Inférieurs à 100
                 10.Recherche d'un nombre par Dichotomie
                 
                
                 Entrez 'q' pour revenir en arrière 
     --->""")
        if choix_fait =='1':
            clear()
            exo6_7_1()
        elif choix_fait =='2':
            clear()
            exo6_7_2()
        elif choix_fait =='3':
            clear()
            exo6_7_3()
        elif choix_fait=='4':
            clear()
            exo6_7_4()
        elif choix_fait=='5':
            clear()
            exo6_7_5()
        elif choix_fait=='6':
            clear()
            exo6_7_6()
        elif choix_fait=='7':
            clear()
            exo6_7_7()
        elif choix_fait=='8':
            clear()
            exo6_7_8()
        elif choix_fait=='9':
            clear()
            exo6_7_9()
        elif choix_fait=='10':
            clear()
            exo6_7_10()
        elif choix_fait[0:1].lower()=='q':
            break
        else:
            clear()
            print('Choix invalide\n\t\tVeuillez suivre les instructions données ...\n')
            continue