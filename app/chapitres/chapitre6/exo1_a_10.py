from core.settings import YOUR_ENV_VARIABLE
from tools.console import *

import random

def exo6_7_1():
    print('\n\t\t\t Jours de la semaine \n')

    semaine = ['Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi','Dimache']

    for jour in semaine:
        if jour in semaine[:4]:
            print(f'{jour:8s} : Au travail ! ')
        if jour == semaine[4]:
            print(f'{jour:8s} : Chouette , c\'est Vendredi ')
        if jour in semaine[5:]:
            print(f'{jour:8s} : Repos ce week-end')
    key()
    clear()

def exo6_7_2():
    print('\n\t\t\t Séquence Complémentaire d\'un Brin d\'ADN\n')

    brin_ADN = ['A', 'C', 'G', 'T', 'T', 'A', 'G', 'C', 'T', 'A', 'A', 'C', 'G']
    print('\n Brin d\'Origine       :', end=" " )
    for base in brin_ADN :
        print(base,end=" ")

    for i,base in enumerate(brin_ADN) :
        if (base =='A'): brin_ADN[i]='T'
        if (base =='T'): brin_ADN[i]='A'
        if (base =='C'): brin_ADN[i]='G'
        if (base =='G'): brin_ADN[i]='C'

    print('\n Brin d\'ADN transcrit :',end=" ")
    for base in brin_ADN :
        print(base,end=" ")
    print()
    key()
    clear()


def exo6_7_3():
    print('\n\t\t\t Minimum d\'une Liste\n')
    liste =[8, 4, 6, 1, 5]

    print('liste : ',end="")
    for valeur in liste:
        print(valeur,end=" ")

    min= liste[0]
    for valeur in liste:
        if(min>valeur): min =valeur
    print(f'\nDans cette liste,la valeur la plus petite est : {min}')

    key()
    clear()


def exo6_7_4():
    print('\n\t\t\t Fréquence des Acides Aminés \n')

    seq_AA= ["A", "R", "A", "W", "W", "A", "W", "A", "R", "W", "W", "R", "A", "G"]

    print("\nSéquence d'acides aminés : ",end="")
    for base in seq_AA:
        print(base,end=" ")

    A_compteur,R_compteur,W_compteur,G_compteur=0,0,0,0

    for base in seq_AA:
        if base=="A": A_compteur+=1
        elif base=="R": R_compteur+=1
        elif base=="W": W_compteur+=1
        elif base=="G": G_compteur+=1
    print("\nDans cette séquence d'acides aminés ")

    print(f"L'Alanine(A) est apparu {A_compteur} fois .")
    print(f"L'Arginine (R) est apparu {R_compteur} fois .")
    print(f"Le Tryptophane (W) est apparu {W_compteur} fois .")
    print(f"La Glycine (G) est apparu {G_compteur} fois .")
    key()
    clear()

def exo6_7_5():
    print("\n\t\t\tNotes et Mentions d'un Étudiant \n")

    notes = [14, 9, 13, 15 ,12]
    minimum,maximum = min(notes),max(notes)
    print(f"\tNotes de l'élèves : ",end="")
    for note in notes :
        print(f"{note} ",end="  ")

    total = 0
    for note in notes :
        total += note
    moyenne = total/len(notes)

    mention = 'Insuffisant'
    if moyenne>=10 and moyenne<12 : mention = 'Passable'
    elif moyenne >=12 and moyenne<14 : mention = 'Assez Bien'
    elif moyenne >=14 : mention = 'Bien'

    print(f"""\n\tNote minimale : {minimum}       Note maximale  : {maximum}
    \tTotal : {total}     Moyenne : {moyenne}     Mention : {mention}""")
    key()
    clear()


def exo6_7_6():
    print("\n\t\t\tNombres Pairs\n")
    pair,impair=[],[]

    for valeur in range(21):
        if (valeur%2==0 and valeur <=10): pair.append(valeur)
        elif(valeur%2==1 and valeur>10 ): impair.append(valeur) 

    print('Pour les nombres de 0 à 20 :')
    print('Les nombres pairs inférieurs ou égaux à 10 sont : ' ,end=" ")
    for valeur in pair:
        print(valeur,end=" ")
    print("\nLes nombres impaires strictement supéreurs à 10 sont : ",end="")
    for valeur in impair:
        print(valeur,end=" ")
    print()
    key()
    clear()


def exo6_7_7():
    print("\n\t\t\t Conjecture de Syracuse \n")

    def syrac(n):
        syrac=[n]        
        while (len(syrac)<50):
            n=n//2 if n%2==0 else n*3+1
            syrac.append(n)
            if (n==1):
                trivial = syrac[-3:]
        return syrac,trivial
    while True:
        n1=input("Entrez un entier entier : ")
        if not n1.isdigit():
            print("\nSaisie invalide \n")
            continue
        else :
            n1=abs(int(n1))
            break
            
    syrac_1,trivial_1= syrac(n1)
    print(f'\nLa suite de Syracuse pour le nombre {n1} est : ', end=' ')
    for valeur in syrac_1:
        print(valeur, end="  ")
    print("\nLes termes qui se répetent sont :",end=" ")
    for valeur in trivial_1:
        print(valeur, end="  ")
    print()
    key()
    
    
    while True:
        n2=input("\n\nEntrez un autre entier :")
        if not n2.isdigit():
            print("\nSaisie invalide \n")
            continue
        else : 
            n2=int(n2)
            break
    syrac_2,trivial_2 = syrac(n2)
    print(f'\nLa suite de Syracuse pour le nombre {n2} est : ', end=' ')
    for valeur in syrac_2:
        print(valeur, end="  ")
    print("\nLes termes qui se répetent sont :",end=" ")
    for valeur in trivial_2:
        print(valeur, end="  ")
    print()
    key()

    while True :
        n3=input("\nEntrez un dernier entier :")
        if not n3.isdigit():
            print("\nSaisie invalide \n")
            continue
        else:
            n3=abs(int(n3))
            break
    syrac_3,trivial_3=syrac(n3)
    print(f'\nLa suite de Syracuse pour le nombre {n3} est : ', end=' ')
    for valeur in syrac_3:
        print(valeur, end="  ")
    print("\nLes termes qui se répetent sont :",end=" ")
    for valeur in trivial_3:
        print(valeur, end="  ")
    print()
    key()

    print("\nAprès analyse de ces Trois suites ",end=",")
    if trivial_1== trivial_2==trivial_3:
        print("La Conjecture de Syracuse est vérifié . Les nombres constituants son cycle Trivial sont :" ,end=" ") 
        for valeur in trivial_3:
            print(valeur, end="  ")
        print()
    else : print("La conjecture de Syracuse n'est pas vérifié")

    key()
    clear()

def exo6_7_8():

    print('\n\t\t Attribution de la Structure Secondaire des acides aminés d\'une protéines \n')

    proteine_1TFE =[[48.6, 53.4],[-124.9, 156.7],[-66.2, -30.8], \
                    [-58.8, -43.1],[-73.9, -40.6],[-53.7, -37.5], \
                    [-80.6, -26.0],[-68.5, 135.0],[-64.9, -23.5], \
                    [-66.9, -45.5],[-69.6, -41.0],[-62.7, -37.5], \
                    [-68.2, -38.3],[-61.2, -49.1],[-59.7, -41.1]]

    


    Phi_parfait, Psi_parfait = -57, -47

    for acide_amine in proteine_1TFE:
        if ((Phi_parfait-30<=acide_amine[0]<=Phi_parfait+30 and Psi_parfait-30<=acide_amine[1]<=Psi_parfait+30)):
            print(f'{acide_amine}   : est en Hélice ')
        else : print(f'{acide_amine}   : n\'est pas en Hélice ')
    

    key()
    clear()


def exo6_7_9():
    print('\n\t\t Nombres Premiers Inferieurs à 100 \n')

    nombre_pemier =[]
    for valeur in range (2,100+1):
        premier = True
        for i in range (2,int(valeur**0.5)+1):
            if valeur %i ==0 : premier = False ; break
        if premier : nombre_pemier.append(valeur)

    print("\nLes nombres Premiers inférieurs à 100 sont :",end=" ")
    for valeur in (nombre_pemier):
        print(valeur, end="  ")
    print()
    key()
    clear()

    print("\n----------------- 2e Mth -----------------")

    

def exo6_7_10():
    print("\n\t\tRecherche par Dichotomie \n")
    key()
    print("Pensez à un nombre entre 1 et 100.")

    debut, fin = 1, 100
    tentatives = 1

    while debut < fin:
        milieu = (debut + fin) // 2
        reponse = input(f"Est-ce votre nombre est-il égal à {milieu} ? [+(pour plus grand)/-(pour plus petit)/=(oui)] ")

        if reponse == '+':
            debut = milieu + 1
        elif reponse == '-':
            fin = milieu - 1
        elif reponse == '=':
            print(f"J'ai trouvé en {tentatives} questions !")
            break
        else:
            print("Réponse invalide. Utilisez les caractères +, -, ou =.")

        tentatives += 1
    key()
    clear()