from tools.console import *
from chapitres.chapitre2.exo1_a_3 import*

def chapitre2():
    print("\n\tDans ce chapitre , nous allons nous familiariser avec les variables ...")
    while True :
        
        choix_fait=input("""Entrez le numero de l'exercice de votre choix :
        
        1. Nombres De Friedman
        2. Prédictions de Résultats (Opérations)
        3. Prédictions de Résultats (Opérations & Conversions de Types)
              
        Entrez 'q' pour revenir au menu principal 
    --->""")
        if choix_fait=='1':
            clear()
            exo2_11_1()
        elif choix_fait=='2':
            clear()
            exo2_11_2()
        elif choix_fait=='3':
            clear()
            exo2_11_3()
        elif choix_fait[0:1].lower()=='q':
            break
        else :            
            clear()
            print('Choix invalide\n\t\tVeuillez suivre les instructions données ...\n')
            continue
