from core.settings import YOUR_ENV_VARIABLE
from tools.console import *

def exo2_11_1():
    print(f"""
            Les nombres de Friedman :
    \nLes nombres de Friedman sont des nombres qui peuvent s'exprimer avec tous leurs chiffres dans une expression Mathématique...
    
    ex :
    7+3⁶  = {7+3**6}  ---> C'est un Nombre de Friedman
    
    (3+4)³ = {(3+4)**3}  ---> C'est un nombre de Friedman 
    
    3⁶ - 5 = {(3**6)-5}  ---> Ce n'est pas un nombre de Friedman 
    
    (1 +2⁸)x5 = {(1+2**8)*5}  ---> C'est un nombre de Friedman 
    
    (2+1⁸)⁷ = {(2+1**8)**7}  ---> C'est un nombre de Friedman""")
    key()
    clear()
    


def exo2_11_2():
    print(f"""

        Prediction de Résultat (Operation):
    
      \n (1+2)**3  --->  27
      \n "Da"*4  --->  \"DaDaDaDa\"
      \n \"Da\"+3  --->   Renvoie une erreur car on ne peut faire une operation (+) sur une variable de type string et un integer 
      \n (\"Pa\"+\"La\")*2 --->  'PaLaPala'
      \n("Da"*4)/2  --->  Renvoie une erreur car on ne peut diviser une variable de type string  
      \n 5/2 --->  2.5
      \n 5//2 ---> 2 (quotient de la division Euclidienne)
      \n 5%2 ---> 1 (Reste de la division Euclidienne )""")
    key()
    clear()
    
def exo2_11_3():
    print(f"""
        Prédiction de resultats (Opération & Conversions de Types)
    \nstr(4)*in("3")  --->  '444'
    \nint("3")+float("3.2")  ---> 6.2
    \nstr(3)*float("3.2")  --->   9.6
    \nstr(3/4)/2  --->  '3/43/4'""")
    key()
    clear()