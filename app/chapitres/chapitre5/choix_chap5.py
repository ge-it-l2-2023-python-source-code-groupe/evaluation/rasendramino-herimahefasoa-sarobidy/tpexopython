from tools.console import *
from chapitres.chapitre5.exo1_a_14 import*


def chapitre5():
    print("\n\t Dans cette partie, nous allons voir le fonctionnement des boucles et des Comparaisons \n")
    while True :
        choix_fait=input("""\t\t Entrez le numero de l'exercice que vous voulez lancer 
                
                 1. Boucles de Base
                 2. Boucle et Jour de la Semaine
                 3. Nombres de 1 à 10 sur une ligne 
                 4. Nombres pairs & Impairs
                 5. Calcul de la Moyenne
                 6. Produit de nombres consécutifs
                 7. Triangle
                 8. Trangle Inversé
                 9. Triangle Gauche 
                 10.Pyramide
                 11. Parcours de matrice
                 12. Parcours de demi-matrice
                 13. Sauts de puce
                 14.Suite de Fibonacci
                
                
                 Entrez 'q' pour revenir en arrière 
     --->""")
        if choix_fait=='1':
            clear()
            exo5_4_1()
        elif choix_fait =='2':
            clear()
            exo5_4_2()
        elif choix_fait=='3':
            clear()
            exo5_4_3()
        elif choix_fait =='4':
            clear()
            exo5_4_4()
        elif choix_fait=='5':
            clear()
            exo5_4_5()
        elif choix_fait =='6':
            clear()
            exo5_4_6()
        elif choix_fait=='7':
            clear()
            exo5_4_7()
        elif choix_fait=='8':
            clear()
            exo5_4_8()
        elif choix_fait=='9':
            clear()
            exo5_4_9()
        elif choix_fait=='10':
            clear()
            pyra()
        elif choix_fait=='11':
            clear()
            exo5_4_11()
        elif choix_fait=='12':
            clear()
            exo5_4_12()
        elif choix_fait =='13':
            clear()
            exo5_4_13()
        elif choix_fait=='14':
            clear()
            exo5_4_14()
        elif choix_fait[0:1].lower()=='q':
            break
        else:
            clear()
            print('Choix invalide\n\t\tVeuillez suivre les instructions données ...\n')
            continue