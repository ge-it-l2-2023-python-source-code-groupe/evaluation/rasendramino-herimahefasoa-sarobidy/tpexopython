from tools.console import *
import random


def exo5_4_1():
    print(f'\t\t\tBoucles de Bases \n liste = vache,souris,levure,bacterie\n\n')

    liste =["vache", "souris", "levure", "bacterie"]

        #premiere Mth avec For
    print('Affichage du contenu en utilisant la boucle \'for\' (1ere mth):')
    for contenu in liste:
        print(contenu)

        #deuxieme Mth avec For
    print('\n\nAffichage du contenu en utilisant la boucle \'for\' (2ere mth):')
    for i in range(4):
        print(liste[i])

        #Affichage du contenu avec la boucle While
    print('\n\nAffichage du contenu en utilisant la boucle \'while\' :')
    i = 0
    while i<4:
        print(liste[i])
        i+=1
    
    key()
    clear()


def exo5_4_2():
    print('\t\t\t Boucles et Jours de La semaine ')

    semaine = ['Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi','Dimanche']

    print("\n Les jours de la semaine sont : " ,end="")
    for jour in semaine:
        print(jour , end="  ")
    print("\n\n Les jours du week-end sont : ",end="")
    i = -2
    while i<0:
        print(semaine[i], end="  ")
        i+=1
    print() 
    key()
    clear()

def exo5_4_3():
    print("\t\t\tNombres de 1 à 10 sur une Seule ligne \n")

    for i in range(10):
        print(i+1,end=" ")
    print()
    key()
    clear()


def exo5_4_4():
    print("\t\t\t Nombres Pairs et Impairs \n")

    impairs = [1, 3, 5, 7, 9, 11, 13, 15, 17, 19, 21]
    print('Liste de nombres impairs :')
    for i in range(len(impairs)):
        print(impairs[i],end=" ")


    pairs =[nb+1 for nb in impairs] 
    print('\n\nListe de nombres pairs créer à partir de la liste de nombres impairs :')
    for i in range(len(impairs)):
        print(pairs[i],end=" ")
    print()

    key()
    clear()

def exo5_4_5():
    print('\t\t\t Calcul de Moyenne \n')
    notes = [14, 9, 6, 8, 12]
    print('Les notes de l\'élève : ' ,end="")
    for note in notes:
        print(note,end="  ")

    total = 0
    for i in range(len(notes)):
        total+= notes[i]
    print(f'\nTotal :{total}\nMoyenne : {total/(len(notes)) :.2f}')
    key()
    clear()

def exo5_4_6():
    print('\t\t\t Produit de Nombres Consécutifs \n')

    pairs= list(range(2,21,2))

    print ('Liste de nombres paires : ',end="")
    for nombre in pairs:
        print(nombre,end="  ")


    print("\nProduit 2 à 2 des valeurs de la liste 'pairs' :")

    prod=[]

    for i in range(len(pairs)-1):
        prod.append(pairs[i]*pairs[i+1]) 
        print(prod[i])
    key()
    clear()


def exo5_4_7():
    print("\n\t\t\t Triangle \n")
    while True:
        hauteur = input("Entrez la hauteur de votre triangle :")
        if not hauteur.isdigit():
            print("\nErreur :Caractère de type entiers attendu ")
            continue
        else :
            hauteur = abs(int(hauteur))
            break

    for i in range (1,hauteur+1):
        print("*"*i)
    key()
    clear()


def exo5_4_8():
    while True:
        hauteur = input("Entrez la hauteur de votre triangle :")
        if not hauteur.isdigit():
            print("\nErreur :Caractère de type entiers attendu ")
            continue
        else :
            hauteur = abs(int(hauteur))
            break
    for i in range (hauteur):
        print("*"*(hauteur-i))
    key()
    clear()

def exo5_4_9():
    print("\n\t\t\tTriangle Gauche ")

    while True:
        hauteur = input("Entrez la hauteur de votre triangle :")
        if not hauteur.isdigit():
            print("\nErreur :Caractère de type entiers attendu ")
            continue
        else :
            hauteur = abs(int(hauteur))
            break
    i = 1
    while i != (hauteur +1):
        print(" "*(hauteur-i),"*"*i)
        i+=1
    
    key()
    clear()
    
def pyra():
    print("\n\t\t\t Pyramide \n")

    while True:
        hauteur = input("Entrez la hauteur de votre triangle :")
        if not hauteur.isdigit():
            print("\nErreur :Caractère de type entiers attendu ")
            continue
        else :
            hauteur = abs(int(hauteur))
            break
    stars=1
    for i in range (1,(hauteur+1)):
        print(" "*(hauteur-i),"*"*stars)
        stars+=2
    key()
    clear()



def exo5_4_11():
    print("\n\t\t\t Parcours de Matrice\n")

    matrice3= [[i + j*3 + 1 for i in range(3)] for j in range(3)]
    matrice5= [[i + j*5 + 1 for i in range(5)] for j in range(5)]
    matrice10= [[i + j*10 + 1 for i in range(10)] for j in range(10)]


    print('Pour la matrice 3x3: \nmatrice3 :') 
    for row in matrice3 :
        for val in row:
            print(f'{val:4d}', end="")
        print()
    print('Parcours matriciel de matrice3 avec la boucle \'for\' :')

    print('\n ligne      colonne')

    for row in range (len(matrice3)):
        for column in range (len(matrice3[row])):
            element = matrice3[row][column]
            print(f'{row+1:4d}        {column+1:4d}     ---->  {element}')

    print('\nParcours matriciel de matrice3 avec la boucle \'while\' :')

    print('\n ligne      colonne')
    row = 0
    while row < len(matrice3):
        column = 0
        while column < len(matrice3[row]):
            element = matrice3[row][column]
            print(f'{row+1:4d}        {column+1:4d}     ---->  {element}')
            column += 1
        row+=1


    key()
    clear()


    print('Pour la matrice 5x5: \nmatrice5 :') 
    for row in matrice5 :
        for val in row:
            print(f'{val:4d}', end="")
        print()
    print('Parcours matriciel de matrice3 avec la boucle \'for\' :')

    print('\n ligne      colonne')

    for row in range (len(matrice5)):
        for column in range (len(matrice5[row])):
            element = matrice5[row][column]
            print(f'{row+1:4d}        {column+1:4d}     ---->  {element}')

    print('\nParcours matriciel de matrice3 avec la boucle \'while\' :')

    print('\n ligne      colonne')
    row = 0
    while row < len(matrice5):
        column = 0
        while column < len(matrice5[row]):
            element = matrice5[row][column]
            print(f'{row+1:4d}        {column+1:4d}     ---->  {element}')
            column += 1
        row+=1


    key()
    clear()
    

    print('Pour la matrice 10x10: \nmatrice10 :') 
    for row in matrice10 :
        for val in row:
            print(f'{val:4d}', end="")
        print()
    print('Parcours matriciel de matrice3 avec la boucle \'for\' :')

    print('\n ligne      colonne')

    for row in range (len(matrice10)):
        for column in range (len(matrice10[row])):
            element = matrice10[row][column]
            print(f'{row+1:4d}        {column+1:4d}     ---->  {element}')

    print('\nParcours matriciel de matrice3 avec la boucle \'while\' :')

    print('\n ligne      colonne')
    row = 0
    while row < len(matrice10):
        column = 0
        while column < len(matrice10[row]):
            element = matrice10[row][column]
            print(f'{row+1:4d}        {column+1:4d}     ---->  {element}')
            column += 1
        row+=1
    key()
    clear()


def exo5_4_12():
    print('\n\t\t\tParcours de démi matrince sans Diagonale \n')

    def parcours_demi_matrice(n):
        matrice= [[i + j*n + 1 for i in range(n)] for j in range(n)]
        
        print(f'Exemple de matrice carrée d\'ordre {n} :')
        for ligne in matrice :
            for val in ligne:
                print(f'{val:4d}', end="")
            print()
        print("\n")
        compteur =0

        print('\n ligne      colonne')
        for ligne in range (len(matrice)):
            for colonne in range (len(matrice[ligne])):
                element = matrice[ligne][colonne]
                if (colonne>ligne):
                    compteur+=1
                    print(f'{ligne+1:4d}        {colonne+1:4d}     ---->  {element}')
        print(f'Pour une matrice {n}x{n} , on a parcouru {compteur} cases\n\n')

    def nbre_de_case_parcourues(n):
        compteur =0
        matrice= [[i + j*n + 1 for i in range(n)] for j in range(n)]
        for ligne in range (len(matrice)):
            for colonne in range (len(matrice[ligne])):
                if (colonne>ligne): compteur+=1
        print(f'Pour une matrice {n}x{n} , on a parcouru {compteur} cases\n')

    print(f'Pour une matrice carrée d\'ordre 3 \n')
    parcours_demi_matrice(3)
    key()
    clear()
    print(f'Pour une matrice carrée d\'ordre 4 \n')
    parcours_demi_matrice(4)
    key()
    clear()
    print(f'Pour une matrice carrée d\'ordre 5 \n')
    parcours_demi_matrice(5)

    key()
    clear()

    print('\n---------------------- 2nd version ----------------------\n')
    print('Nombres de cases parcourues : \n')

    for n in range (2,11):
        nbre_de_case_parcourues(n)

    print('''Après une étude sur les resultats ci-dessus , on peut en déduire une formule donnant le nombres de cases parcourues en fonctions de l\'ordre de la matrice 
        
        Formule : Nbre de case parcourues =  n(n-1)/2         avec n = ordre de la matrice 
                    ''')
    
    key()
    clear()

def exo5_4_13():
    print("\n\t\t\tSaut de Puce \n")

   

    position , arrivee, sauts = 0,5,0

    while (position != arrivee ):
        saut = random.choice([-1,1])
        position += saut
        sauts+=1
    print(f'\n La puce est finalement arrivée après avoir fait {sauts} sauts...')
    key()
    clear()


def exo5_4_14():
    print("\n\t\t\t Suite de Fibonacci\n")

    fibo = [0,1]

    for i in range (2,15):
        fibo.append(fibo[i-1]+ fibo[i-2])

    print("\nLes 15 premeiers éléments de la suite de Fibonacci sont :",end="")
    for nombre in fibo :
        print(nombre ,end=" ")

    print('\n\n\n')

    for i in range(2,len(fibo)):
        print(f'Rapport entre le terme n°: {i:2d} et le terme n°: {i-1:2d} ==> {fibo[i] / fibo[i-1]:.3f}')

    print("\nCe rapport tend vers une constante à peu près égale à 1,618 . Cette constante est appelé 'Nombre d'Or' .")

    key()
    clear()
