from core.settings import YOUR_ENV_VARIABLE
from tools.console import *

def exo3_6_1 ():
    print("""
            Affichge dans l'interpreteur et Dans un Programme :
    
    
    \n  Dans l'interpreteur Python :
>>> 1+1
2 
L'interpreteur Python affiche le resultat de l'opération de suite après l'éxécution de la ligne '1+1' 
    
      \n  Dans un script test.py :
# test.py
1+1

Après éxécution du script test.py :
    bash :
user@PC :~/chemin/vers/le/script/à/executer $  python test.py
user@PC :~/chemin/vers/le/script/à/executer $   

      
Le script 'test.py' a été correctement éxécuter mais n'affiche rien du tout car pour afficher quelque chose durant l'éxecution d'un script .py , il est impératif d'utiliser la fonction 'print()'

""")
    key()
    clear()

def exo3_6_2():
    print ("\n\n\t\tPoly-A")


    poly_A= 'A'*20

    print('Brin d\'ADN poly-A :\n',poly_A)

    key()
    clear()

def exo3_6_3():
    print("\n\n\t\t Poly-A et Poly-CG")

    poly_A = 'A'*20
    poly_GC = 'GC'*20
    print('\nBrin d\'ADN poly-A (20 bases) suivi d\'un poly-GC regulier(40 bases): ')
    print(poly_A+poly_GC)
    key()
    clear()

def exo3_6_4():
    print("\t\t\t Écriture formatée ")

    a , b, c = 'salut', 102 , 10.318
    print('Affichage en une seule ligne des variables a , b et c :')
    print (f'a:{a}  b:{b}   c:{c:.2f}')
    key()
    clear()

def exo3_6_5():
    print("\t\t\tÉcriture formatée 2 :")

    perc_GC = ((4500 + 2575)/14800)*100

    print('\nAffichage de perc_GC avec 0,1,2 et 3 decimales :\n ')

    print(f'Le pourcentage de CG est {perc_GC:.0f} %')
    print(f'Le pourcentage de CG est {perc_GC:.1f} %')
    print(f'Le pourcentage de CG est {perc_GC:.2f} %')
    print(f'Le pourcentage de CG est {perc_GC:.3f} %')
    key()
    clear()
