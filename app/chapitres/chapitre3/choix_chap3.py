from tools.console import *
from chapitres.chapitre3.exo1_a_5 import *


def chapitre3():
    print("\n\tDans cette partie du devoir, nous allons en savoir un peu plus sur les Methodes d'affichages en Python\n ")

    while True :
        choix_fait=input("""\t\t Entrez le numero de l'exercice que vous voulez lancer 
                
                 1. Affichage dans L'interpreteur et Dans un programme
                 2. Poly-A
                 3. Poly-A et Poly-GC
                 4. Écriture Formatée
                 5. Écriture Formatée (2)
                
                
                 Entrez 'q' pour revenir en arrière 
     --->""")
        if choix_fait=='1':
            clear()
            exo3_6_1()
        elif choix_fait=='2':
            clear()
            exo3_6_2()
        elif choix_fait=='3':
            clear()
            exo3_6_3()
        elif choix_fait=='4':
            clear()
            exo3_6_4()
        elif choix_fait=='5':
            clear()
            exo3_6_5()
        elif choix_fait[0:1].lower()=='q':
            break
        else:
            clear()
            print('Choix invalide\n\t\tVeuillez suivre les instructions données ...\n')
            continue