from core.settings import YOUR_ENV_VARIABLE
from tools.console import *


def exo4_10_1():
    print("\t\t\t Jours de la Semaine :")

    semaine = ['Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi','Dimanche']
    print('Les jours de la semaine sont : ',end='')
    for jour in semaine:
        print(jour,end=' ')
    print()
    

        #récuppération des des 5 premiers jours et le week-end 
    # Par l'indiçage positve 

    premiers_jours, week_end = semaine[:5], semaine[5:]
    print('\nLes 5 premiers jours de la semaine :')
    for jour in premiers_jours:
        print(jour,end=" ")
    print()
    print('\nLe week-end :',end=" ")
    for jour in week_end:
        print(jour,end=" ")

    #par indiçage négative 
    print('\n\n****** Résultat de l\'autre Methode pour la récupération des jours de la semaines ******')
    premiers_jours ,week_end = semaine[-7:5],semaine[-2:]
    print('\nLes 5 premiers jours de la semaine :')
    for jour in premiers_jours:
        print(jour,end=" ")
    print()
    print('\nLe week-end :',end=" ")
    for jour in week_end:
        print(jour,end=" ")


    # Récupération du dernier jour de la semaine 
    derner_jour = semaine[6]
    print(f"\n\nLe dernier jour de la semaine est : {derner_jour}")

    print('\n\n****** Résultat de la récupération du dernier jour de la semaine via une autre méthode ******')
    dernier_jour = semaine[-1]
    print(f" Le dernier jour de la semaine est : {derner_jour}")


    #Inversion des jours de la semaine 
    semaine_inversee = semaine[::-1]
    print('\n\n Jours de la semaine inversés : ')
    for jour in semaine_inversee:
        print(jour, end="  ")
    print()
    key()
    clear()



def exo4_10_2():

    print('\t\t\t Saisons ')

    hiver,printemps,ete,automne = ['Juin','Juillet','Août'] \
                                ,['Septembre','Octobre','Novembre']\
                                ,['Décembre','Janvier','Février']\
                                ,['Mars','Avril','Mai']

    saisons = [hiver,printemps,ete,automne ]

    print("""
        saisons : hiver,printemps,ete,automne 
        
        
        Printemps : Septembre, octobre, novembre
        Été : Décembre, janvier, février
        Automne : Mars, avril, mai
        Hiver : Juin, juillet, août
        """)
    print("""
            \nRésultats 
        
        saisons[2]  --->  ['Décembre','Janvier','Février'] (contenu Ete qui est la troisieme valeur de Saisons)
        \nsaisons[1][0]  --->   ['Septembre']  (Premiere valeur de Printemps étant la Deuxieme valeur de Saisons)
        \nsaisons[1:2]  --->   ['Septembre','Octobre','Novembre'] (contenu de la valeur à l'index numero 1 de Saisons)
        \nsaisons[:][1]  --->  ['Septembre', 'Octobre', 'Novembre']  
                Explication : 
        'saisons[:] ' renvoie tout le contenu de saisons 
        avec l'ajout de '[1]' , elle permet de prendre la valeur de saisons à l'index 1 , c'est à dire le contenu de printemps""")
    key()
    clear()

def exo4_10_3():
    print("\t\t\t Table de multiplication par 9")

    table_9 = list(i*9 for i in range(0,11))
    for i, val in enumerate(table_9):
        print(f"{i+1} x 9 = {val}")
    key()
    clear()

def exo4_10_4():
    print('\t\t\t Nombres pairs')

    print(f"\n\nDans l'intervalle [2,10000] inclus , il y a {len(list(range(2, 10001, 2)))} nombres paires")
    key()
    clear()


def exo4_10_5():
    print("\n\t\t\t Liste & Indice \n")

    semaine = ['Lundi','Mardi','Mercredi','Jeudi','Vendredi','Samedi','Dimanche']

    print('Contenu de la liste Semaine :' , end="")
    for jour in semaine : print(jour,end="  ")

    print("\n\n Le contenu de la liste semaine à l'indice 4 est : " , semaine[4])

    semaine[0], semaine[len(semaine)-1] = semaine[len(semaine)-1], semaine[0]

    print('\n\nLa Liste semaine après échange de la première et la dernière valeur :')
    for jour in semaine:
        print(jour , end=" ")

    semaine[0], semaine[len(semaine)-1] = semaine[len(semaine)-1], semaine[0]

    print(" \n\nRépétition 12 fois du dernier élément de la liste :")

    for i in range(12):
        print(semaine[len(semaine)-1],end="  ")
    print()
    key()
    clear()


def exo4_10_6():
    print("\n\t\t\t List & Range \n")

    lstVide=[];lstFlottant=[0.0]*5

    print(f'Liste vide : {lstVide}')
    print(f'Liste de flottants : {lstFlottant}')


    #Ajout des nombres de 1 à 1000 avec pas de 200
    lstVide = list(range(0,(1000+1),200))


    # Afficher les entiers de 0 à 3
    print('\nEntiers de 0à 4:')
    print(list(range(0,4)))


    # Afficher les entiers de 4 à 7
    print('Entierss entre 4 et 7:')
    print(list(range(4,7)))


    # Afficher les entiers de 2 à 8 par pas de 2
    print('Entiers de 2 à 8 avec pas de 2')
    print(list(range(2,(8+1),2)))


    print('\n\nLstElmt:')
    lstElmt = list(range(0,5))
    for element in lstElmt :
        print(element,end ="  ")


    print('\nLstElmt après ajout des deux liste lstVide et lstFlottant :')
    #Pour ajouter des element à une liste, we use .extend
    lstElmt.extend(lstVide)
    lstElmt.extend(lstFlottant)
    for element in lstElmt :
        print(element,end ="  ")

    print()
    key()
    clear()