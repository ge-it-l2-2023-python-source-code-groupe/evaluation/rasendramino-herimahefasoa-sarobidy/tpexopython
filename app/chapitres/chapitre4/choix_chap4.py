from tools.console import *
from chapitres.chapitre4.exo1_a_6 import *

def chapitre4():
    print("\n\t Nous allons entamer la parite concernant Les listes et les Indiçage .\n C'est super, non 😇?")
    while True :
        choix_fait=input("""\t\t Entrez le numero de l'exercice que vous voulez lancer 
                
                 1. Jour de la Semaine
                 2. Saisons
                 3. Table de Multiplisatin de 9
                 4. Nombres Pairs
                 5. List & Indice
                 6. List & Range
                
                
                 Entrez 'q' pour revenir en arrière 
     --->""")
        if choix_fait =='1':
            clear()
            exo4_10_1()
        elif choix_fait =='2':
            clear()
            exo4_10_2()
        elif choix_fait =='3':
            clear()
            exo4_10_3()
        elif choix_fait=='4':
            clear()
            exo4_10_4()
        elif choix_fait=='5':
            clear()
            exo4_10_5()
        elif choix_fait=='6':
            clear()
            exo4_10_6()
        elif choix_fait[0:1].lower()=='q':
            break
        else:
            clear()
            print('Choix invalide\n\t\tVeuillez suivre les instructions données ...\n')
            continue