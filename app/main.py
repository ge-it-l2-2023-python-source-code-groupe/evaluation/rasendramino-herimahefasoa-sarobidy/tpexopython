from tools.console import * 
from chapitres.chapitre2.choix_chap2 import *
from chapitres.chapitre3.choix_chap3 import *
from chapitres.chapitre4.choix_chap4 import *
from chapitres.chapitre5.choix_chap5 import *
from chapitres.chapitre6.choix_chap6 import *
    
def choix():
    
        while True:
            choix_fait=(input('''       Exercices sur :
                    1. Les variables
                    2. L'affichage
                    3. Les Listes 
                    4. Les Boucles & Comparaisons
                    5. Les Tests
                            
                    Veuillez entrez 'q' si vous voulez quitter le programme
                
            --->'''))
            
            
            
            if (choix_fait =='1'):
                clear() 
                chapitre2()

            if (choix_fait =='2'):
                clear()
                chapitre3()

            if (choix_fait =='3'):
                clear()
                chapitre4()

            if (choix_fait =='4'):
                clear()
                chapitre5()

            if (choix_fait=='5'):
                clear()
                chapitre6()
            
            
        
            if(choix_fait[0:1].lower()=='q'): 
                print('\tAu revoir alors 😀 Merci d\'être passé ')
                exit()           

            
            else : 
                clear()
                print(' \nVeuillez saisir une option parmis celles proposées ')
                

if __name__=="__main__":
     
    clear()
    print('''   Bien le bonjour Monsieur 😇 
          \nVeuillez faire votre choix suite à la partie du devoir que vous voulez tester :''')
    choix()